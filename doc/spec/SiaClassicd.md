# SiaClassic-UI SiaClassicd lifecycle specification

## Introduction

The purpose of this spec is to outline the desired behaviour of SiaClassic-UI as it relates to starting, stopping, or connecting to an existing SiaClassicd.

## Desired Functionality

- SiaClassic-UI should check for the existence of a running daemon on launch, by calling `/daemon/version` using the UI's current config.
If the daemon isn't running, SiaClassic-UI should launch a new siaclassicd instance, using the bundled siaclassicd binary.  If a bundled binary cannot be found, prompt the user for the location of their `siaclassicd`.  SiaClassicd's lifetime should be bound to SiaClassic-UI, meaning that `/daemon/stop` should be called when SiaClassic-UI is exited.
- Alternatively, if an instance of `siaclassicd` is found to be running when SiaClassic-UI starts up, SiaClassic-UI should not quit the daemon when it is exited.

This behaviour can be implemented without any major changes to the codebase by leveraging the existing `detached` flag.

## Considerations

- Calling `/daemon/version` using the UI's config does not actually tell you whether or not there is an active `siaclassicd` running on the host, since a different `siaclassicd` instance could be running using a bindaddr different than the one specified in `config`.
