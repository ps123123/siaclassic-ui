# SiaClassic-UI Plugin API Specification

## Introduction

This specification outlines the functionality that SiaClassic-UI's plugin API exposes to developers.

## Functionality

The SiaClassic-UI Plugin api exposes a simple interface for making API calls to siaclassicd, creating file dialogs, displaying notifications, and displaying error messages.  This interface is assigned to the `window` object of each plugin, and has the following functions:

- `SiaClassicAPI.call()`, a wrapper to the configured `siaclassic.js`'s `.apiCall` function.
- `SiaClassicAPI.config`, the current SiaClassic-UI config.
- `SiaClassicAPI.hastingsToSiaClassiccoins`, conversion function from hastings to siaclassiccoins.  Returns a `BigNumber` and takes either a `BigNumber` or `string`.
- `SiaClassicAPI.siaclassiccoinsToHastings`, conversion function from siaclassiccoins to hastings.
- `SiaClassicAPI.openFile(options)`, a wrapper which calls Electron.dialog.showOpenDialog with `options`.
- `SiaClassicAPI.saveFile(options)`, a wrapper which calls Electron.dialog.showSaveDialog with `options`.
- `SiaClassicAPI.showMessage(options)`, a wrapper which calls Electron.showMessageBox with `options`.
- `SiaClassicAPI.showError(options)`, a wrapper which calls Electron.showErrorBox with `options`.
