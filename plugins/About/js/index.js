'use strict'
import { platform } from 'os'
import { shell } from 'electron'

// If dev enable window reload
if (process.env.NODE_ENV === 'development') {
  require('electron-css-reload')()
}

// Set UI version via package.json.
document.getElementById('uiversion').innerHTML = VERSION

// Set daemon version via API call.
SiaClassicAPI.call('/daemon/version', (err, result) => {
  if (err) {
    SiaClassicAPI.showError('Error', err.toString())
  } else {
    document.getElementById('siaclassicversion').innerHTML = result.version
  }
})

function genDownloadLink (version, thePlatform) {
  let plat = thePlatform
  if (plat === 'darwin') {
    plat = 'osx'
  }

  return `https://gitlab.com/moderndevgroup/SiaClassic-UI/releases/download/v${version}/SiaClassic-UI-v${version}-${plat}-x64.zip`
}

function updateCheck () {
  SiaClassicAPI.call('/daemon/update', (err, result) => {
    if (err) {
      SiaClassicAPI.showError('Error', err.toString())
    } else if (result.available) {
      document.getElementById('newversion').innerHTML = result.version
      document.getElementById('downloadlink').href = genDownloadLink(
        result.version,
        platform()
      )
      document.getElementById('nonew').style.display = 'none'
      document.getElementById('yesnew').style.display = 'block'
    } else {
      document.getElementById('nonew').style.display = 'block'
      document.getElementById('yesnew').style.display = 'none'
    }
  })
}

document.getElementById('updatecheck').onclick = updateCheck
document.getElementById('datadiropen').onclick = () => {
  shell.showItemInFolder(SiaClassicAPI.config.siaclassicd.datadir)
}
