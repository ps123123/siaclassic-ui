// loadingScreen.js: display a loading screen until communication with SiaClassicd has been established.
// if an available daemon is not running on the host,
// launch an instance of siaclassicd using config.js.
import { remote, shell } from 'electron'
import * as SiaClassicd from 'siaclassic.js'
import Path from 'path'
import React from 'react'
import ReactDOM from 'react-dom'
import StatusBar from './statusbar'

const dialog = remote.dialog
const app = remote.app
const fs = remote.require('fs')
const config = remote.getGlobal('config')
const siaclassicdConfig = config.attr('siaclassicd')

const spinner = document.getElementById('loading-spinner')
const overlay = document.getElementsByClassName('overlay')[0]
const overlayText = overlay
  .getElementsByClassName('centered')[0]
  .getElementsByTagName('p')[0]
const errorLog = document.getElementById('errorlog')
overlayText.textContent = 'Loading SiaClassic...'

const showError = error => {
  overlayText.style.display = 'none'
  errorLog.textContent = 'A critical error loading SiaClassic has occured: ' + error
  errorLog.style.display = 'inline-block'
  spinner.style.display = 'none'
}

// startUI starts a SiaClassic UI instance using the given welcome message.
// calls initUI() after displaying a welcome message.
const startUI = (welcomeMsg, initUI) => {
  // Display a welcome message, then initialize the ui
  overlayText.innerHTML = welcomeMsg

  // Construct the status bar component and poll for updates from SiaClassicd
  const updateSyncStatus = async function () {
    try {
      const consensusData = await SiaClassicd.call(siaclassicdConfig.address, {
        timeout: 500,
        url: '/consensus'
      })
      const gatewayData = await SiaClassicd.call(siaclassicdConfig.address, {
        timeout: 500,
        url: '/gateway'
      })
      ReactDOM.render(
        <StatusBar
          peers={gatewayData.peers.length}
          synced={consensusData.synced}
          blockheight={consensusData.height}
        />,
        document.getElementById('statusbar')
      )
      await new Promise(resolve => setTimeout(resolve, 5000))
    } catch (e) {
      await new Promise(resolve => setTimeout(resolve, 500))
      console.error('error updating sync status: ' + e.toString())
    }
    updateSyncStatus()
  }

  updateSyncStatus()

  initUI(() => {
    overlay.style.display = 'none'
  })
}

// checkSiaClassicPath validates config's SiaClassic path.  returns a promise that is
// resolved with `true` if siaclassicdConfig.path exists or `false` if it does not
// exist.
const checkSiaClassicPath = () =>
  new Promise(resolve => {
    fs.stat(siaclassicdConfig.path, err => {
      if (!err) {
        resolve(true)
      } else {
        resolve(false)
      }
    })
  })

// unexpectedExitHandler handles an unexpected siaclassicd exit, displaying the error
// piped to siaclassicd-output.log.
const unexpectedExitHandler = () => {
  try {
    const errorMsg = fs.readFileSync(
      Path.join(siaclassicdConfig.datadir, 'siaclassicd-output.log')
    )
    showError('SiaClassicd unexpectedly exited. Error log: ' + errorMsg)
  } catch (e) {
    showError('SiaClassicd unexpectedly exited.')
  }
}

// Check if SiaClassicd is already running on this host.
// If it is, start the UI and display a welcome message to the user.
// Otherwise, start a new instance of SiaClassicd using config.js.
export default async function loadingScreen (initUI) {
  // Create the SiaClassic data directory if it does not exist
  try {
    fs.statSync(siaclassicdConfig.datadir)
  } catch (e) {
    fs.mkdirSync(siaclassicdConfig.datadir)
  }
  // If SiaClassic is already running, start the UI with a 'Welcome Back' message.
  const running = await SiaClassicd.isRunning(siaclassicdConfig.address)
  if (running) {
    startUI('Welcome back', initUI)
    return
  }

  // check siaclassicdConfig.path, if it doesn't exist optimistically set it to the
  // default path
  if (!await checkSiaClassicPath()) {
    siaclassicdConfig.path = config.defaultSiaClassicdPath
  }

  // check siaclassicdConfig.path, and ask for a new path if siaclassicd doesn't exist.
  if (!await checkSiaClassicPath()) {
    // config.path doesn't exist.  Prompt the user for siaclassicd's location
    dialog.showErrorBox(
      'SiaClassicd not found',
      "SiaClassic-UI couldn't locate siaclassicd.  Please navigate to siaclassicd."
    )
    const siaclassicdPath = dialog.showOpenDialog({
      title: 'Please locate siaclassicd.',
      properties: ['openFile'],
      defaultPath: Path.join('..', siaclassicdConfig.path),
      filters: [{ name: 'siaclassicd', extensions: ['*'] }]
    })
    if (typeof siaclassicdPath === 'undefined') {
      // The user didn't choose siaclassicd, we should just close.
      app.quit()
    }
    siaclassicdConfig.path = siaclassicdPath[0]
  }

  // Launch the new SiaClassicd process
  //FIX sia-directory => siaclassic...
  try {
    const siaclassicdProcess = SiaClassicd.launch(siaclassicdConfig.path, {
      'siaclassic-directory': siaclassicdConfig.datadir,
      'rpc-addr': siaclassicdConfig.rpcaddr,
      'host-addr': siaclassicdConfig.hostaddr,
      'api-addr': siaclassicdConfig.address,
      modules: 'cghrtw'
    })
    siaclassicdProcess.on('error', e =>
      showError('SiaClassicd couldnt start: ' + e.toString())
    )
    siaclassicdProcess.on('close', unexpectedExitHandler)
    siaclassicdProcess.on('exit', unexpectedExitHandler)
    window.siaclassicdProcess = siaclassicdProcess
  } catch (e) {
    showError(e.toString())
    return
  }

  // Set a timeout to display a warning message about long load times caused by rescan.
  setTimeout(() => {
    if (overlayText.textContent === 'Loading SiaClassic...') {
      overlayText.innerHTML =
        'Loading can take a while after upgrading to a new version. Check the <a style="text-decoration: underline; cursor: pointer" id="releasenotelink">release notes</a> for more details.'

      document.getElementById('releasenotelink').onclick = () => {
        shell.openExternal('https://gitlab.com/moderndevgroup/SiaClassic/releases')
      }
    }
  }, 30000)

  // Wait for this process to become reachable before starting the UI.
  const sleep = (ms = 0) => new Promise(r => setTimeout(r, ms))
  while ((await SiaClassicd.isRunning(siaclassicdConfig.address)) === false) {
    await sleep(500)
  }
  // Unregister callbacks
  window.siaclassicdProcess.removeAllListeners('error')
  window.siaclassicdProcess.removeAllListeners('exit')
  window.siaclassicdProcess.removeAllListeners('close')

  startUI('Welcome to SiaClassic', initUI)
}
