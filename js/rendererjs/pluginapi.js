// pluginapi.js: SiaClassic-UI plugin API interface exposed to all plugins.
// This is injected into every plugin's global namespace.
import * as SiaClassicd from 'siaclassic.js'
import { remote } from 'electron'
import React from 'react'
import DisabledPlugin from './disabledplugin.js'
import Path from 'path'

const dialog = remote.dialog
const mainWindow = remote.getCurrentWindow()
const config = remote.getGlobal('config')
const siaclassicdConfig = config.siaclassicd
const fs = remote.require('fs')
let disabled = false

const sleep = (ms = 0) => new Promise(r => setTimeout(r, ms))

window.onload = async function () {
  // ReactDOM needs a DOM in order to be imported,
  // but the DOM is not available until the plugin has loaded.
  // therefore, we have to global require it inside the window.onload event.

  /* eslint-disable global-require */
  const ReactDOM = require('react-dom')
  /* eslint-enable global-require */

  let startSiaClassicd = () => {}

  const renderSiaClassicdCrashlog = () => {
    // load the error log and display it in the disabled plugin
    let errorMsg = 'SiaClassicd exited unexpectedly for an unknown reason.'
    try {
      errorMsg = fs.readFileSync(
        Path.join(siaclassicdConfig.datadir, 'siaclassicd-output.log'),
        { encoding: 'utf-8' }
      )
    } catch (e) {
      console.error('error reading error log: ' + e.toString())
    }

    document.body.innerHTML =
      '<div style="width:100%;height:100%;" id="crashdiv"></div>'
    ReactDOM.render(
      <DisabledPlugin errorMsg={errorMsg} startSiaClassicd={startSiaClassicd} />,
      document.getElementById('crashdiv')
    )
  }

  //FIX sia-directory => siaclassic...
  startSiaClassicd = () => {
    const siaclassicdProcess = SiaClassicd.launch(siaclassicdConfig.path, {
      'siaclassic-directory': siaclassicdConfig.datadir,
      'rpc-addr': siaclassicdConfig.rpcaddr,
      'host-addr': siaclassicdConfig.hostaddr,
      'api-addr': siaclassicdConfig.address,
      modules: 'cghrtw'
    })
    siaclassicdProcess.on('error', renderSiaClassicdCrashlog)
    siaclassicdProcess.on('close', renderSiaClassicdCrashlog)
    siaclassicdProcess.on('exit', renderSiaClassicdCrashlog)
    window.siaclassicdProcess = siaclassicdProcess
  }
  // Continuously check (every 2000ms) if siaclassicd is running.
  // If siaclassicd is not running, disable the plugin by mounting
  // the `DisabledPlugin` component in the DOM's body.
  // If siaclassicd is running and the plugin has been disabled,
  // reload the plugin.
  while (true) {
    const running = await SiaClassicd.isRunning(siaclassicdConfig.address)
    if (running && disabled) {
      disabled = false
      window.location.reload()
    }
    if (!running && !disabled) {
      disabled = true
      renderSiaClassicdCrashlog()
    }
    await sleep(2000)
  }
}

window.SiaClassicAPI = {
  call: function (url, callback) {
    SiaClassicd.call(siaclassicdConfig.address, url)
      .then(res => callback(null, res))
      .catch(err => callback(err, null))
  },
  config: config,
  hastingsToSiaClassiccoins: SiaClassicd.hastingsToSiaClassiccoins,
  siaclassiccoinsToHastings: SiaClassicd.siaclassiccoinsToHastings,
  openFile: options => dialog.showOpenDialog(mainWindow, options),
  saveFile: options => dialog.showSaveDialog(mainWindow, options),
  showMessage: options => dialog.showMessageBox(mainWindow, options),
  showError: options => dialog.showErrorBox(options.title, options.content)
}
